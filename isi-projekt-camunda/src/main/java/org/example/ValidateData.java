package org.example;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.logging.Logger;

public class ValidateData implements JavaDelegate {
    private final Logger LOGGER = Logger.getLogger(ValidateData.class.getName());

    public void execute(DelegateExecution execution) {
        LOGGER.info("started ValidateData");

        String exchange_to = (String) execution.getVariable("to");
        String exchange_from = (String) execution.getVariable("from");
        Double amount = (Double) execution.getVariable("amount");

        boolean isValid = validate(exchange_to, exchange_from, amount);
        execution.setVariable("isValid", isValid);
        LOGGER.info("isValid: " + isValid);
    }

    private boolean validate(String to, String from, Double amount) {
        if (to == null || from == null || to.equals(from)) {
            return false;
        }
        if (amount == null || amount <= 0) {
            return false;
        }
        return true;
    }
}
