package org.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.logging.Logger;

public class GetCurrency implements JavaDelegate {

    private final Logger LOGGER = Logger.getLogger(GetCurrency.class.getName());

    @Override
    public void execute(DelegateExecution execution) throws IOException {
        LOGGER.info("Started GetCurrency");

        String exchange_to = (String) execution.getVariable("to");
        String exchange_from = (String) execution.getVariable("from");
        Double amount = (Double) execution.getVariable("amount");

        String URL = "http://localhost:8081/make-request?to=" + exchange_to + "&from=" + exchange_from + "&amount=" + amount;
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(URL, String.class);
        // LOGGER.info("result: " + result);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(result);
        LOGGER.info("rootNode: " + rootNode);

        double conversionRate = rootNode.path("payload").path("conversion_rates").path(exchange_from).asDouble();
        execution.setVariable("conversionRate", conversionRate);
        LOGGER.info("conversionRate: " + conversionRate);
    }
}
