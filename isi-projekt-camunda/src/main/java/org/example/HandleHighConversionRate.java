package org.example;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import java.util.logging.Logger;

public class HandleHighConversionRate implements JavaDelegate {
    private final Logger LOGGER = Logger.getLogger(HandleHighConversionRate.class.getName());

    @Override
    public void execute(DelegateExecution execution) {
        LOGGER.info("Handling high conversion rate");
    }
}
